<?php
require_once("phpmailer/class.phpmailer.php");
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
// $mailSend = false;
$rest_json = file_get_contents("php://input");
$_POST = json_decode($rest_json, true);

// var_dump($_POST);
if ( isset($_POST['contactType']) ) {

    $name = $_POST['name'];
    $contactType = $_POST['contactType'];
    $phone = $_POST['phone'] != '' ? $_POST['phone'] : 'k.A';
    $email = $_POST['mail'] != '' ? $_POST['mail'] : 'k.A';
    $message = $_POST['message'] != '' ? $_POST['message'] : 'k.A';

    // auskommentieren vor livegang
    // $receipient = 's.hrkalovic@birkenbeul.com';
    $receipient = "bav@stuttgarter.de";

    define("SMTP_HOST", "mail.your-server.de");
    define("SMTP_PORT", 587);
    define("NOREPLY", "noreply@bavheute.de");

    // define("SMTP_HOST", "gate.stuttgarter.de");
    // define("SMTP_PORT", 2525);
    // define("NOREPLY", "noreply@stuttgarter.de");


    // ** EMAIL AN ABSENDER ***********************************************************************************
    $mail = new phpmailer();
    // $mail->SMTPDebug = 1;  // debugging: 1 = errors and messages, 2 = messages only

    $mail->CharSet 	= 'utf-8';

    $mail->isSMTP();

    $mail->SMTPAuth     = true;
    $mail->SMTPSecure   = 'tls';
    $mail->Host         = SMTP_HOST;
    $mail->Port         = SMTP_PORT;
    $mail->Username     = "noreply@bavheute.de";
    $mail->Password     = "r9ACvW30hk81V5on";
    // $mail->SMTPAuth = false;

    $mail->From     = NOREPLY;
    $mail->FromName = "noreply";
    $mail->AddAddress($receipient);
    $mail->WordWrap = 50;                              // Zeilenumbruch einstellen
    $mail->IsHTML(true);                               // als HTML-E-Mail senden    


    $timestamp = time();
    $formatedTime = date("d.m.Y - H:i", $timestamp);
    $htmlmsg = '<html><body>Name: '.$name.'<br>Datum: '.$formatedTime.'<br>Anliegen: '.$contactType.'<br>Nachricht: '.$message.'<br>E-Mail: '.$email.'<br>Telefon: '.$phone.'</body></html>';
    $textmsg = 'Name: '.$name.'\n\nDatum: '.$formatedTime.'\n\nAnliegen: '.$contactType.'\n\nNachricht: '.$message.'\n\nE-Mail: '.$email.'\n\nTelefon: '.$phone;
    echo $htmlmsg;
    // $mail = new PHPMailer;
    // $mail->CharSet 	= 'utf-8';
    $mail->setFrom(NOREPLY, 'noreply');
    $mail->Subject  = 'Nachricht aus digitaler bAV Lösung ('.$contactType.')';
    $mail->Body     = $htmlmsg;
    $mail->AltBody  = $textmsg;
    if(!$mail->send()) {
        echo 'Message was not sent.';
        echo 'Mailer error: ' . $mail->ErrorInfo;
    } else {
    // echo 'Message has been sent.';
    // $mailSend = true;
    echo 'OK';
    }
}
?>
